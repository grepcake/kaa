#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore/QElapsedTimer>
#include <QtCore/QFileSystemWatcher>
#include <QtCore/QFutureWatcher>
#include <QtWidgets/QProgressDialog>
#include <atomic>
#include <memory>
#include <mutex>

#include <absl/container/flat_hash_map.h>

#include "file_index.hpp"

namespace Ui {
    class MainWindow;
}

class main_window : public QMainWindow {
    Q_OBJECT

    enum state {
        NO_OPERATIONS, CANCELLED, ERROR,
        GATHERING_FILES, INDEXING, INDEXING_FINISHED,
        SEARCHING, SEARCHING_FINISHED
    };

    using Searcher = std::boyer_moore_horspool_searcher<std::string::iterator>;

  public:
    explicit main_window(QWidget *parent = nullptr);
    ~main_window() final;

  private slots:
    void show_about_dialog();
    void clear();
    void cancel();
    void select_directory();
    void state_cancelled();
    void state_error();
    void state_gathering(const QString &dir);
    void state_indexing();
    void state_indexing_finished();
    void state_searching(QString const &text);
    void state_searching_finished();
    void display_found(std::string const &filename, std::vector<kaa::file_index::entry> const &entries);

  signals:
    void operation_cancelled();
    void error_encountered();
    void gathered_files(std::size_t count);
    void gathering_finished();
    void found_in_file(std::string const &filename, std::vector<kaa::file_index::entry> entries);

  private:
    QFileSystemWatcher filesystem_watcher;
    QFutureWatcher<void> indexing_watcher;
    std::vector<std::filesystem::directory_entry> entries;
    absl::flat_hash_map<std::string, kaa::file_index> indices;
    std::mutex mutex;

    QFutureWatcher<void> searching_watcher;
    std::string needle;
    Searcher searcher{needle.begin(), needle.end()};

    std::atomic_bool cancelled{false};
    std::atomic_bool idle{true};

    QProgressDialog progress_dialog;
    std::unique_ptr<Ui::MainWindow> ui;

    QElapsedTimer indexing_timer;
    QElapsedTimer searching_timer;
};

#endif // MAINWINDOW_H
