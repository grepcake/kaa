#ifndef KAA_INDEX_H
#define KAA_INDEX_H

#include <atomic> // atomic_bool
#include <filesystem>
#include <fstream> // ifstream

#include "absl/container/flat_hash_set.h"

namespace kaa {
    constexpr size_t max_line_length = 2000;
    constexpr size_t max_file_size   = 1 << 30;

    using trigram_t  = std::uint32_t;
    using trigrams_t = absl::flat_hash_set<trigram_t>;

    struct file_index {
        struct entry {
            entry(std::size_t line_no, std::size_t char_no, std::size_t symbol_no)
                : line_no(line_no), char_no(char_no), symbol_no(symbol_no) {}

            std::size_t line_no;
            std::size_t char_no;
            std::size_t symbol_no;
        };

        file_index() = default;
        explicit file_index(std::filesystem::directory_entry entry, std::atomic_bool const &cancelled);
        file_index(file_index &&) = default;

        bool empty() const { return trigrams.empty(); }

        template <typename Searcher>
        std::vector<entry>
        lookup(std::string const &needle, Searcher const &searcher, std::atomic_bool const &cancelled) const {
            if (not has_trigrams_of(needle)) {
                return {};
            }

            std::vector<entry> found;
            std::ifstream ifs(file.path());
            std::array<char, max_line_length> buffer{};
            std::size_t line_no = 0;
            while (ifs.getline(buffer.data(), max_line_length)) {
                auto end = buffer.begin() + ifs.gcount() - 1;
                auto it  = buffer.begin();
                while (true) {
                    if (cancelled) {
                        return {};
                    }
                    it = std::search(it, end, searcher);
                    if (it == end) {
                        break;
                    }
                    auto char_no = std::size_t(it - buffer.begin());
                    found.emplace_back(make_entry(line_no, char_no, buffer.data()));
                    ++it;
                }
                ++line_no;
            }
            return found;
        }

        bool
        has_trigrams_of(std::string const &needle) const;

        void
        update(std::atomic_bool const &cancelled);

      private:
        entry make_entry(std::size_t line_no, std::size_t char_no, char const *line) const;

      public:
        std::filesystem::directory_entry file;
        trigrams_t trigrams;
    };
} // namespace kaa

#endif //KAA_INDEX_H
