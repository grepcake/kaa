#include "utf8_tools.hpp"

#include "file_index.hpp"

namespace fs = std::filesystem;
using std::size_t;

namespace kaa {
    namespace {
        constexpr trigram_t make_trigram(char ch1, char ch2, char ch3) {
            trigram_t result = 0;
            result           = (result << CHAR_BIT) + (unsigned char)ch1;
            result           = (result << CHAR_BIT) + (unsigned char)ch2;
            result           = (result << CHAR_BIT) + (unsigned char)ch3;
            return result;
        }

        void add_trigrams(trigrams_t &trigrams, char const *buffer, size_t count) {
            if (count < 3) {
                return;
            }
            trigrams.reserve(trigrams.size() + count - 2);
            for (std::size_t i = 0; i < count - 2; ++i) {
                trigrams.emplace(make_trigram(buffer[i], buffer[i + 1], buffer[i + 2]));
            }
        }
    } // namespace

    file_index::file_index(fs::directory_entry path, const std::atomic_bool &cancelled)
        : file(std::move(path)) {
        update(cancelled);
    }

    bool file_index::has_trigrams_of(std::string const &needle) const {
        if (needle.size() < 3) {
            return false;
        }
        for (std::size_t i = 0; i < needle.size() - 2; ++i) {
            trigram_t trigram = make_trigram(needle[i], needle[i + 1], needle[i + 2]);
            if (not trigrams.contains(trigram)) {
                return false;
            }
        }
        return true;
    }

    void file_index::update(std::atomic_bool const &cancelled) {
        if (cancelled.load(std::memory_order_relaxed)) {
            trigrams.clear();
            return;
        }

        std::error_code ec;
        if (file.file_size(ec) > max_file_size) {
            trigrams.clear();
            return;
        }

        std::array<char, max_line_length> buffer{};
        std::ifstream ifs(file.path());
        if (not ifs) {
            trigrams.clear();
            return;
        }

        auto state = UTF8_ACCEPT;
        while (ifs.getline(buffer.data(), max_line_length)) {
            if (cancelled.load(std::memory_order_relaxed)) {
                return;
            }
            if (validate_utf8(&state, buffer.data(), size_t(ifs.gcount())) == UTF8_REJECT) {
                trigrams.clear();
                return;
            }
            add_trigrams(trigrams, buffer.data(), size_t(ifs.gcount() - 1));
        }
        if (not ifs.eof()) {
            trigrams.clear();
        }
    }

    file_index::entry file_index::make_entry(std::size_t line_no, std::size_t char_no, const char *line) const {
        auto symbol_no = find_symbol_no(line, char_no);
        return {line_no, char_no, symbol_no};
    }

} // namespace kaa
