#include <gtest/gtest.h>

#include "file_index.hpp"
#include "utf8_tools.hpp"

namespace {
    bool is_valid(std::string const &str) {
        auto state = UTF8_ACCEPT;
        validate_utf8(&state, str.c_str(), str.length());
        return state == UTF8_ACCEPT;
    }
} // namespace

TEST(utf8_tools, valid_utf8) {
    EXPECT_TRUE(is_valid("plain ascii text\n123"));
    EXPECT_TRUE(is_valid("зима недолго злилась — её пора"));
    EXPECT_TRUE(is_valid("mix up русский с english и französich… µ"));
}

TEST(utf8_tools, invalid_utf8) {
    EXPECT_FALSE(is_valid("\x3d\x84"));
}

TEST(utf_tools, symbol_no) {
    EXPECT_EQ(find_symbol_no("once, upon a midnight dreary", 10), 10);
    EXPECT_EQ(find_symbol_no("  О, не выходи из комнаты. Танцуй, поймав, боссанову", 35), 21);
    std::string mixed = "mix up русский с english и französich… µ";
    auto pos = mixed.find("ö");
    EXPECT_EQ(find_symbol_no(mixed.c_str(), pos), 32);
}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
